<?php


use Tests\TestCase;
use Viewflex\InviteDataParser\InviteDataParser;
use Viewflex\InviteDataParser\Util\DataFactory;

class InviteDataParserUnitTest extends TestCase
{

    public function test_phone_number_validation()
    {
        $this->assertTrue(InviteDataParser::isValidPhoneNumber('1234567890'));
        $this->assertFalse(InviteDataParser::isValidPhoneNumber('123456789'));
        $this->assertFalse(InviteDataParser::isValidPhoneNumber('12345678901'));
    }

    public function test_email_address_validation()
    {
        $this->assertTrue(InviteDataParser::isValidEmailAddress('user@example.com'));
        $this->assertFalse(InviteDataParser::isValidEmailAddress('user@example,com'));
        $this->assertFalse(InviteDataParser::isValidEmailAddress('user @ example.com'));
        $this->assertFalse(InviteDataParser::isValidEmailAddress('user.example.com'));
        $this->assertFalse(InviteDataParser::isValidEmailAddress('@example.com'));
        $this->assertFalse(InviteDataParser::isValidEmailAddress(' @example.com'));
    }

    public function test_date_string_validation()
    {
        $this->assertTrue(InviteDataParser::isValidDate('2018-01-30'));
        $this->assertFalse(InviteDataParser::isValidDate('2008-6-30'));
        $this->assertFalse(InviteDataParser::isValidDate('2008/06/30'));
    }

    public function test_time_string_validation()
    {
        $this->assertTrue(InviteDataParser::isValidTime('01:02:03'));
        $this->assertFalse(InviteDataParser::isValidTime('1:02:03'));
    }

    public function test_parses_valid_csv()
    {
        $csv = DataFactory::getSpecData();
        $response = InviteDataParser::transformFileData($csv);
        $this->assertEquals(1, $response->getSuccess());
        $this->assertEquals(10, count($response->getData()));
    }

    public function test_rejects_csv_with_invalid_header_row()
    {
        $csv = DataFactory::getDataWithInvalidHeaderRow();
        $response = InviteDataParser::transformFileData($csv);
        $this->assertEquals(0, $response->getSuccess());
        $this->assertEquals('Invalid header row.', $response->getMessage());

    }

    public function test_rejects_csv_with_an_invalid_date_format()
    {
        $csv = DataFactory::getDataWithAnInvalidDateFormat();
        $response = InviteDataParser::transformFileData($csv);
        $this->assertEquals(0, $response->getSuccess());
        $this->assertEquals('Caught invalid date format.', $response->getMessage());
    }

    public function test_rejects_csv_with_an_invalid_time_format()
    {
        $csv = DataFactory::getDataWithAnInvalidTimeFormat();
        $response = InviteDataParser::transformFileData($csv);
        $this->assertEquals(0, $response->getSuccess());
        $this->assertEquals('Caught invalid time format.', $response->getMessage());
    }

}
