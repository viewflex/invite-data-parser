<?php

namespace Viewflex\InviteDataParser;


use Illuminate\Support\ServiceProvider;

class InviteDataParserServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->loadViewsFrom(__DIR__.'/Resources/views', 'invite-data-parser');
		$this->publishes([
			__DIR__ . '/Resources/views' => base_path('resources/views/vendor/invite-data-parser'),
			__DIR__ . '/Resources/js/components' => base_path('resources/js/components')
		], 'invite-data-parser');
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register()
	{
		require __DIR__ . '/routes.php';
    }

}
