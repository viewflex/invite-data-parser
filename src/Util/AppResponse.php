<?php

namespace Viewflex\InviteDataParser\Util;


/**
 * Provides a standard rich response format for use anywhere in the app.
 */
class AppResponse
{

    /**
     * @return int
     */
    public $success;

    /**
     * @return string
     */
    public $message;

    /**
     * @return mixed
     */
    public $data;

    /**
     * @param int $success
     * @param string $message
     * @param mixed $data
     */
    public function __construct($success = 0, $message = null, $data = null)
    {
        $this->success = $success;
        $this->message = $message;
        $this->data = $data;
    }

    /**
     * @return int
     */
    public function getSuccess()
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'success' => $this->success,
            'message' => $this->message,
            'data' => $this->data
        ];
    }

}
