<?php

namespace Viewflex\InviteDataParser;


use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Viewflex\InviteDataParser\Util\AppResponse;

class InviteDataParser
{
    /**
     * @var int
     */
    protected $num_days;

    /**
     * @var string
     */
    protected $current_date;

    /**
     * Create parser with specified parameters or defaults.
     * 
     * @param int $num_days
     * @param string $current_date
     */
    public function __construct($num_days = 7, $current_date = '2018-03-05')
    {
        $this->num_days = $num_days;
        $this->current_date = $current_date;
    }
    
    /**
     * Using input 'data_file', returns response with proper data array (or error).
     * Input data may be file input from form, or base64 file in json-encoded body.
     * 
     * @param Request $request
     * @return AppResponse
     */
    static public function getFileData($request)
    {
        try {

            $filename = strval(time()).'.csv';
            $subdir = 'csv';

            if ($request->hasFile('data_file')) {

                // We have file input from a form, check the file type.
                $uploaded_file = $request->file('data_file');
                if ($uploaded_file->clientExtension() !== 'csv') {
                    return new AppResponse(0, 'Invalid input file type.');
                }

                // Create file in local storage.
                $request->data_file->storeAs($subdir, $filename);

            } elseif (($request->has('data_file') && ($request->isJson()))) {

                // We have file input data from an ajax request, check the format.
                $data = explode(',', $request->input('data_file'));
                if ($data[0] !== 'data:text/csv;base64') {
                    return new AppResponse(0, 'File data not in expected format.');
                }
                $csv_text = base64_decode($data[1]);

                // Create file in local storage.
                Storage::put($subdir.'/'.$filename, $csv_text);

            } else {
                return new AppResponse(0, 'No form or json input for file data.');
            }

            // Log new file creation.
            $stored_filepath = storage_path('app/'.$subdir.'/'.$filename);
            Log::info('Stored incoming csv data as: '.$stored_filepath);

            // Load the csv data into array.
            $csv = file(storage_path('app/'.$subdir.'/'.$filename));

        } catch (\Exception $e) {
            Log::error($msg = 'Caught exception in getFileData(): ' . $e->getMessage());
            return new AppResponse(0, $msg);
        }

        if (! is_array($csv)) {
            return new AppResponse(0, 'Invalid file data format.');
        }

        return self::transformFileData($csv);
    }

    /**
     * Validates and converts raw csv data to a properly formatted array.
     *
     * @param array $csv
     * @return AppResponse
     */
    static public function transformFileData($csv)
    {
        $expected_header_row = 'trans_type,trans_date,trans_time,cust_num,cust_fname,cust_email,cust_phone';
        if (trim($csv[0]) !== $expected_header_row) {
            return new AppResponse(0, 'Invalid header row.', trim($csv[0]));
        }

        try {
            $raw = array_map('str_getcsv', $csv);
            $data = self::headerRowToKeys($raw);
        } catch (\Exception $e) {
            Log::error($msg = 'Caught exception in transformFileData(): ' . $e->getMessage());
            return new AppResponse(0, $msg);
        }

        // Make sure data and time are in expected format, they're used for sorting.
        foreach($data as $row) {

            if (! self::isValidDate($row['trans_date'])) {
                return new AppResponse(0, 'Caught invalid date format.');
                break;
            }

            if (! self::isValidTime($row['trans_time'])) {
                return new AppResponse(0, 'Caught invalid time format.');
                break;
            }

        }

        return new AppResponse(1, '', $data);
    }    
    
    /**
     * Returns enhanced array including 'sent' flag, invite channel, invite type, and status message.
     *
     * @param array $data
     * @return AppResponse
     */
    public function processData($data)
    {
        
        try {

            // Create new array with additional columns.
            $processed_data = [];
            $count = count($data);
            for($i = 0; $i < $count; $i++) {
                $processed_data[$i] = [
                    'orig_index' => $i,
                    'trans_type' => $data[$i]['trans_type'],
                    'trans_date' => $data[$i]['trans_date'],
                    'trans_time' => $data[$i]['trans_time'],
                    'cust_num' => $data[$i]['cust_num'],
                    'cust_fname' => $data[$i]['cust_fname'],
                    'cust_email' => $data[$i]['cust_email'],
                    'cust_phone' => $data[$i]['cust_phone'],
                    'invite_sent' => '',
                    'invite_channel' => '',
                    'invite_type' => '',
                    'message' => ''
                ];
            }

            // Sort transactions by timestamp, oldest first.
            usort($processed_data, function ($a, $b) {
                return strcmp(($a['trans_date'].' '.$a['trans_time']), ($b['trans_date'].' '.$b['trans_time']));
            });

            $emails = $texts = [];
            $to_date = Carbon::createFromFormat('Y-m-d', $this->current_date);
            $from_date = Carbon::createFromFormat('Y-m-d H:i:s', ($this->current_date . ' 00:00:00'))->subDays($this->num_days-1);

            // Loop through transactions, deciding which notifications to send, populating new columns.
            foreach($processed_data as &$transaction) {

                $trans_date = Carbon::createFromFormat('Y-m-d H:i:s', ($transaction['trans_date'] . ' 00:00:00'));

                if (! $trans_date->between($from_date, $to_date)) {
                    $transaction['invite_sent'] = 'no';
                    $transaction['message'] = 'out of date range';
                } else {

                    if (self::isValidPhoneNumber($transaction['cust_phone'])) {

                        // If a text was not already sent to that phone...
                        if (! in_array($transaction['cust_phone'], $texts)) {
                            $texts[] = $transaction['cust_phone'];
                            $transaction['invite_sent'] = 'yes';
                            $transaction['invite_channel'] = 'text';
                            $transaction['invite_type'] = $transaction['trans_type'];
                            $transaction['message'] = 'ok';
                        } else {
                            $transaction['invite_sent'] = 'no';
                            $transaction['message'] = 'text already sent';
                        }

                    } elseif (self::isValidEmailAddress($transaction['cust_email'])) {

                        // If an email was not already sent to that address...
                        if (! in_array($transaction['cust_email'], $emails)) {
                            $emails[] = $transaction['cust_email'];
                            $transaction['invite_sent'] = 'yes';
                            $transaction['invite_channel'] = 'email';
                            $transaction['invite_type'] = $transaction['trans_type'];
                            $transaction['message'] = 'ok';
                        } else {
                            $transaction['invite_sent'] = 'no';
                            $transaction['message'] = 'email already sent';
                        }

                    } else {

                        // No phone or email.
                        $transaction['invite_sent'] = 'no';
                        $transaction['message'] = 'no valid contact info';

                    }

                }

            }

            unset($transaction);
            
        } catch (\Exception $e) {
            Log::error($msg = 'Caught exception in processData(): ' . $e->getMessage());
            return new AppResponse(0, $msg);
        }

        return new AppResponse(1, '', $processed_data);
    
    }

    /**
     * Formats data from csv file, using column header values as keys.
     *
     * @param array $data
     * @return array
     */
    static public function headerRowToKeys($data)
    {
        array_walk($data, function(&$a) use ($data) {
            $a = array_combine($data[0], $a);
        });
        array_shift($data); # remove column header
        return $data;
    }
    
    /**
     * @param string $str
     * @return bool
     */
    static public function isValidDate($str)
    {
        $validator = Validator::make(['date' => $str], ['date' => 'required|date_format:Y-m-d']);
        return $validator->passes();
    }

    /**
     * @param string $str
     * @return bool
     */
    static public function isValidTime($str)
    {
        return ((strlen($str) == 8) && (substr($str, 2, 1) == ':') && (substr($str, 5, 1) == ':'));
    }

    /**
     * @param string $str
     * @return bool
     */
    static public function isValidPhoneNumber($str)
    {
        preg_match_all('!\d+!', $str, $matches);
        $phone = implode('', $matches[0]);
        return strlen($phone) == 10;
    }

    /**
     * @param string $str
     * @return bool
     */
    static public function isValidEmailAddress($str)
    {
        // Because Laravel's email validation, strangely, allows spaces.
        if (strpos($str, ' ') !== false) {
            return false;
        }

        $validator = Validator::make(['email' => $str], ['email' => 'required|email']);
        return $validator->passes();
    }

}
