<?php

namespace Viewflex\InviteDataParser;


use App\Http\Controllers\Controller;

class InviteDataParserController extends Controller
{
    
    /**
     * Receives uploaded csv data, returning response array with parsed data or error.
     *
     * @return array
     */
    public function parse()
    {
        // Create parser, with custom parameters if specified.
        if (request()->has('num_days', 'current_date')) {
            $parser = new InviteDataParser(strval(request()->input('num_days')), request()->input('current_date'));
        } else {
            $parser = new InviteDataParser();
        }

        // Get response with csv data from file or ajax inout.
        $response = $parser->getFileData(request());

        if (! $response->getSuccess()) {
            // Return response array with error.
            return $response->toArray();
        }

        // Return response array with parsed data or error.
        return $parser->processData($response->getData())->toArray();
    }

}
