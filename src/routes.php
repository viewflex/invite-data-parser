<?php

app()->router->get('invite-data-parser/upload', function () {
    return view('invite-data-parser::upload');
});

app()->router->post('invite-data-parser/parse', [
    'as' => 'invite-data-parser.parse',
    'uses' => '\Viewflex\InviteDataParser\InviteDataParserController@parse',
    'middleware' => 'api'
]);
