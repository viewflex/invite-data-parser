// DataParserComponent.js

import React, { Component } from 'react';
import axios, { post } from 'axios';
import DataDisplayComponent from './DataDisplayComponent';

export default class DataParserComponent extends Component
{
   constructor(props) {
      super(props);
      this.state = {
          dataFile: null,
          success: 0,
          message: '',
          parsedData: null,
      }
      this.onFormSubmit = this.onFormSubmit.bind(this)
      this.onChange = this.onChange.bind(this)
      this.fileUpload = this.fileUpload.bind(this)
    }
    onFormSubmit(e){
      e.preventDefault() 
      this.fileUpload();
    }
    onChange(e) {
      let files = e.target.files || e.dataTransfer.files;
      if (!files.length)
            return;
      this.createFile(files[0]);
    }
    createFile(dataFile) {
      let reader = new FileReader();
      reader.onload = (e) => {
        this.setState({
          dataFile: e.target.result
        })
      };
      reader.readAsDataURL(dataFile);
    }
    fileUpload(){
      const url = this.props.endpoint;
      const formData = {data_file: this.state.dataFile}

      post(url, formData)
      .then(response => {
        console.log(response.data.data);
        this.setState({ 
          success: response.data.success,
          message: response.data.message,
          parsedData: response.data.data 
        });
      })
      .catch(function (error) {
        console.log(error);
      })
      .finally(function () {
        // always executed
      });
    }
   render()
   {
      return(
          <div>
              <h1>Invite Data Parser</h1>
              <form onSubmit={this.onFormSubmit}>
                  <p>{(this.state.dataFile == null) ? 'Select a CSV file to process.' : 'Click Upload to parse the file.'}</p>
                  <input type="file" name="data_file" id="data_file" onChange={this.onChange} />
                  <button type="submit">Upload</button>
              </form>
              <br/>
              <DataDisplayComponent data={(this.state.parsedData)}/>
              <p>{ this.state.message }</p>
          </div>
      )
   }
}
