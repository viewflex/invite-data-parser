import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import DataParserComponent from './DataParserComponent';

export default class Upload extends Component {
    render() {
        return (
            <div>
                <DataParserComponent endpoint='/invite-data-parser/parse' />
            </div>
        );
    }
}

if (document.getElementById('upload')) {
    ReactDOM.render(<Upload></Upload>, document.getElementById('upload'));
}
