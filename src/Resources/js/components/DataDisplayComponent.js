//DataDisplayComponent.js

import React, { Component } from 'react';

export default class DataDisplayComponent extends Component
{
    constructor(props) {
        super(props);
    }

    displayData(){

      return (
          <div id="data-display">
              <table border="1" align="center" cellPadding="5" cellSpacing="0">
                  <thead>
                      <tr>
                          <th>trans_type</th>
                          <th>trans_date</th>
                          <th>trans_time</th>
                          <th>cust_num</th>
                          <th>cust_fname</th>
                          <th>cust_email</th>
                          <th>cust_phone</th>
                          <th>invite_sent</th>
                          <th>invite_channel</th>
                          <th>invite_type</th>
                          <th>message</th>
                      </tr>
                  </thead>
                  <tbody>
              {this.props.data.map(
                  row =>
                      <tr id={row.orig_index} key={row.orig_index}>
                          <td>{row.trans_type}</td>
                          <td>{row.trans_date}</td>
                          <td>{row.trans_time}</td>
                          <td>{row.cust_num}</td>
                          <td>{row.cust_fname}</td>
                          <td>{row.cust_email}</td>
                          <td>{row.cust_phone}</td>
                          <td>{row.invite_sent}</td>
                          <td>{row.invite_channel}</td>
                          <td>{row.invite_type}</td>
                          <td>{row.message}</td>
                      </tr>
              )}
                  </tbody>
              </table>
            </div>
      )
      
    }


   render()
   {

      if(this.props.data == null){
        return 'no data'
      } else {
        return this.displayData()
      }
      
   }
}
