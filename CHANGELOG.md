# Changelog

This document tracks changes in release versions of **InviteDataParser**. This project adheres to the [Semantic Versioning](http://semver.org/spec/v2.0.0.html) standard.

## Version [0.0.4](https://bitbucket.org/viewflex/invite-data-parser/src/0.0.4/) - 2019-05-22

### Added

React upload form page.

## Version [0.0.3](https://bitbucket.org/viewflex/invite-data-parser/src/0.0.3/) - 2019-05-20

### Added

Handling of ajax request and more exceptions.

## Version [0.0.2](https://bitbucket.org/viewflex/invite-data-parser/src/0.0.2/) - 2019-05-15

### Added

- Support for custom date and range parameters.

## Version [0.0.1](https://bitbucket.org/viewflex/invite-data-parser/src/0.0.1/) - 2019-05-15

Initial release.
