# Invite Data Parser

Takes an uploaded CSV file of customer transaction data and decides what invite notifications to send, returning the original data with additional columns.

## System Requirements

Laravel framework version 5.2 or higher.

## Installation

In your application's `composer.json` file, add the values displayed below:

```json
{
    "require": {
        "viewflex/invite-data-parser": "dev-master"
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:viewflex/invite-data-parser.git"
        }
    ]
}
```
Then run this command to install the package:

```bash
composer update
```

### Register the Service Provider

After installing, add the `InviteDataParserServiceProvider` to the list of service providers in `config/app.php`. If you are using Laravel version 5.5 or greater with package discovery enabled, this step is not necessary.

```php
Viewflex\InviteDataParser\InviteDataParserServiceProvider::class,
```

### Resources (optional)

These steps are necessary only if you intend to use the included React upload form.

#### Publish Resources

Run this command to publish the JavaScript files (and the form page template):

```bash
php artisan vendor:publish  --tag='invite-data-parser'
```

#### Configure and Compile Resources

If the Laravel project has been scaffolded for React, you should see a line like this in `/resources/assets/js/app.js`: 

```php
require('./components/Example');
```

Change the component referenced:

```php
require('./components/Upload');
```

Then run this command to compile the resources:

```bash
npm run dev'
```

### Allowing CORS Requests (optional)

If you will be accessing the parsing endpoint from a domain other than that of your Laravel project, you can install the `barryvdh/laravel-cors` package to avoid handling CORS issues manually.

```bash
composer require barryvdh/laravel-cors
```


## Routing

These routes represent the upload form and parsing API call:

```php
app()->router->get('invite-data-parser/upload', function () {
    return view('invite-data-parser::upload');
});

app()->router->post('invite-data-parser/parse', [
    'as' => 'invite-data-parser.parse',
    'uses' => '\Viewflex\InviteDataParser\InviteDataParserController@parse',
    'middleware' => 'api'
]);
```

## Usage

Once installed and set up, go to the `invite-data-parser/upload` url in your browser. The form allows you to choose a file and click Upload to have it validated and parsed. The file must be valid CSV and otherwise conform to the specifications, or an error message will be returned. If all is successful, you should see a table with parsed data.

![screenshot](parser-ui.png)

## Architecture

A POST request is sent to the parsing endpoint with a file upload parameter named `data_file`. This could be a standard file input within submitted form data, or a parameter within a json-encoded request body; both ways are handled transparently.

A response array should be received containing `success`, `message`, and `data` elements. Optionally, include custom parameters `num_days` and `current_date` in the request, otherwise the hard-coded testing defaults will be used.

A successful parsing operation should return something like this:

```json
{
    "success": 1,
    "message": "",
    "data": [
        {
            "orig_index": 8,
            "trans_type": "service",
            "trans_date": "2018-01-02",
            "trans_time": "21:00:00",
            "cust_num": "10019",
            "cust_fname": "Jerri Sanders",
            "cust_email": "",
            "cust_phone": "5526691245",
            "invite_sent": "no",
            "invite_channel": "",
            "invite_type": "",
            "message": "out of date range"
        },
        {
            "orig_index": 7,
            "trans_type": "sales",
            "trans_date": "2018-02-27",
            "trans_time": "20:00:00",
            "cust_num": "10018",
            "cust_fname": "Jacob",
            "cust_email": "",
            "cust_phone": "",
            "invite_sent": "no",
            "invite_channel": "",
            "invite_type": "",
            "message": "no valid contact info"
        },
        {
            "orig_index": 2,
            "trans_type": "sales",
            "trans_date": "2018-03-01",
            "trans_time": "11:20:00",
            "cust_num": "10012",
            "cust_fname": "Bobby",
            "cust_email": "bob@gmail.com",
            "cust_phone": "123-122-1223",
            "invite_sent": "yes",
            "invite_channel": "text",
            "invite_type": "sales",
            "message": "ok"
        },
        {
            "orig_index": 0,
            "trans_type": "sales",
            "trans_date": "2018-03-01",
            "trans_time": "13:00:00",
            "cust_num": "10012",
            "cust_fname": "Bob",
            "cust_email": "bob1@gmail.com",
            "cust_phone": "123-123-1234",
            "invite_sent": "yes",
            "invite_channel": "text",
            "invite_type": "sales",
            "message": "ok"
        },
        {
            "orig_index": 1,
            "trans_type": "service",
            "trans_date": "2018-03-01",
            "trans_time": "14:00:00",
            "cust_num": "10013",
            "cust_fname": "Bob",
            "cust_email": "bob2@gmail.com",
            "cust_phone": "123-234-2345",
            "invite_sent": "yes",
            "invite_channel": "text",
            "invite_type": "service",
            "message": "ok"
        },
        {
            "orig_index": 3,
            "trans_type": "sales",
            "trans_date": "2018-03-01",
            "trans_time": "16:00:00",
            "cust_num": "10014",
            "cust_fname": "Robert",
            "cust_email": "",
            "cust_phone": "123568556",
            "invite_sent": "no",
            "invite_channel": "",
            "invite_type": "",
            "message": "no valid contact info"
        },
        {
            "orig_index": 6,
            "trans_type": "service",
            "trans_date": "2018-03-01",
            "trans_time": "19:00:00",
            "cust_num": "10017",
            "cust_fname": "Jane",
            "cust_email": "jane@gmail",
            "cust_phone": "111-222-3333",
            "invite_sent": "yes",
            "invite_channel": "text",
            "invite_type": "service",
            "message": "ok"
        },
        {
            "orig_index": 4,
            "trans_type": "sales",
            "trans_date": "2018-03-02",
            "trans_time": "17:30:00",
            "cust_num": "10015",
            "cust_fname": "Jill",
            "cust_email": "jill@gmail,com",
            "cust_phone": "",
            "invite_sent": "no",
            "invite_channel": "",
            "invite_type": "",
            "message": "no valid contact info"
        },
        {
            "orig_index": 5,
            "trans_type": "service",
            "trans_date": "2018-03-02",
            "trans_time": "18:00:00",
            "cust_num": "10016",
            "cust_fname": "Jillian",
            "cust_email": "",
            "cust_phone": "224-225-2228",
            "invite_sent": "yes",
            "invite_channel": "text",
            "invite_type": "service",
            "message": "ok"
        },
        {
            "orig_index": 9,
            "trans_type": "service",
            "trans_date": "2018-03-04",
            "trans_time": "14:00:00",
            "cust_num": "10020",
            "cust_fname": "Bobby",
            "cust_email": "bob@gmail.com",
            "cust_phone": "123-122-1223",
            "invite_sent": "no",
            "invite_channel": "",
            "invite_type": "",
            "message": "text already sent"
        }
    ]
}
```

If there was a problem, you should receive a response like this:

```json
{
    "success": 0,
    "message": "No input file.",
    "data": null
}
```


## Tests

### PHPUnit

Tests extend the `TestCase` class installed with Laravel. When using Laravel 5.4 or greater, this line is necessary. With prior Laravel versions, this line should be commented out:


```php
    use Tests\TestCase;
```

Run the `phpunit` executable from the `invite-data-parser/tests` directory. You may have installed phpunit elsewhere, but to run the copy installed with Laravel, execute this command:

	./../../../../vendor/bin/phpunit
	

### Postman

Using an HTTP client such as [Postman](https://www.getpostman.com/), you can test the endpoint directly. A [Postman collection file](tests/Postman/InviteDataParser.postman_collection.json) is included in this package's `tests` directory; you can run the collection's test suite from within Postman or on the command line via Newman (see Postman docs). Set the collection variable `domain` to your Laravel app's actual domain (instead of 'http://example.com').


## Changelog

Release versions are tracked in the [Changelog](CHANGELOG.md).
